# -*- coding: utf-8 -*-

from math import sqrt

class Point3D(object):
    r"""
    A 3D point.
    """

    def __init__(self, x, y, z):
        r"""
        Creates a 3D point with the given coordinates.

        >>> point = Point3D(4.0, 5.0, -2.0)
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Point3D(4.0, 5.0, -2.0)
        Point3D(4.0, 5.0, -2.0)
        """
        return 'Point3D(%s, %s, %s)' % (self.x, self.y, self.z)

    def __add__(self, vector):
        r"""
        Returns the point obtained by translating self by vector.

        >>> Point3D(4.0, 5.0, -2.0) + Vector3D(1.0, -1.0, 2.0)
        Point3D(5.0, 4.0, 0.0)
        """
        return Point3D(self.x + vector.x, self.y + vector.y, self.z + vector.z)

    def __sub__(self, other):
        r"""
        Returns the vector from self to other.

        >>> Point3D(4.0, 5.0, -2.0) - Point3D(1.0, -1.0, 2.0)
        Vector3D(3.0, 6.0, -4.0)
        """
        return Vector3D(self.x - other.x, self.y - other.y, self.z - other.z)

class Vector3D(object):

    def __init__(self, x, y, z):
        r"""
        Creates an instance of 3D vector.

        >>> v = Vector3D(1.0, 2.0, -3.0)
        """
        self.x = x
        self.y = y
        self.z = z

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Vector3D(1.0, 2.0, -3.0)
        Vector3D(1.0, 2.0, -3.0)
        """
        return 'Vector3D(%s, %s, %s)' % (self.x, self.y, self.z)

    def __rmul__(self, scalar):
        r"""
        Multiplies self by the given scalar.

        >>> 4.0 * Vector3D(1.0, 2.0, -3.0)
        Vector3D(4.0, 8.0, -12.0)
        """
        return Vector3D(scalar * self.x, scalar * self.y, scalar * self.z)

    def is_zero(self):
        r"""
        Returns True if and only if self is the null vector.

        >>> Vector3D(0.0, 0.0, 0.0).is_zero()
        True
        >>> Vector3D(1.0, 0.0, 0.0).is_zero()
        False
        """
        return self.x == 0 and self.y == 0 and self.z == 0

    def _potential_proportional_constant(self, other):
        r"""
        Returns the first well-defined ratio between self and other's
        coordinates.
        """
        if other.x != 0:
            return self.x / other.x
        elif other.y != 0:
            return self.y / other.y
        elif other.z != 0:
            return self.z / other.z
        return 0.0

    def has_same_direction(self, other, and_orientation=False):
        r"""
        Returns True if and only if self and other are in the same directions.

        If ``and_orientation`` is set to True, check also if the vectors point
        in the same direction.

        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(2.0, 4.0, 6.0))
        True
        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(-3.0, -6.0, -9.0))
        True
        >>> Vector3D(1.0, 2.0, 3.0).has_same_direction(Vector3D(1.0, 3.0, 2.0))
        False
        >>> Vector3D(0.0, 0.0, 0.0).has_same_direction(Vector3D(1.0, 3.0, 1.0))
        True
        """
        if self.is_zero() or other.is_zero(): return True
        k = self._potential_proportional_constant(other)
        return (not and_orientation or k >= 0) and\
               self.x == k * other.x and\
               self.y == k * other.y and\
               self.z == k * other.z

    def dot_product(self, other):
        r"""
        Returns the dot product of self with other.
        >>> Vector3D(0.0, 0.0, 0.0).dot_product(Vector3D(1.0, 3.0, 1.0))
        0.0
        >>> Vector3D(1.0, 2.0, 3.0).dot_product(Vector3D(-7.0, 6.0, 2.0))
        11.0
        """
        return self.x * other.x + self.y * other.y + self.z * other.z

    def square_norm(self):
        r"""
        Returns the square norm of self.

        >>> Vector3D(1.0, 2.0, 3.0).square_norm()
        14.0
        >>> Vector3D(0.0, 0.0, 0.0).square_norm()
        0.0
        """
        return self.dot_product(self)

    def norm(self):
        r"""
        Returns the norm of self.

        >>> Vector3D(1.0, 2.0, 3.0).norm() == sqrt(14.0)
        True
        >>> Vector3D(0.0, 0.0, 0.0).norm()
        0.0
        """
        return sqrt(self.square_norm())

    def cross_product(self, other):
        r"""
        Returns the cross product of self with other.

        >>> Vector3D(1.0, 2.0, 3.0).cross_product(Vector3D(-1.0, 2.0, 1.0))
        Vector3D(-4.0, -4.0, 4.0)
        >>> Vector3D(0.0, 0.0, 0.0).cross_product(Vector3D(-1.0, 2.0, 1.0)).is_zero()
        True
        """
        return Vector3D(self.y * other.z - self.z * other.y,
                        self.z * other.x - self.x * other.z,
                        self.x * other.y - self.y * other.x)

class Line3D(object):

    def __init__(self, point, direction):
        r"""
        Creates an instance of a 3D line.

        >>> line = Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        """
        self.point = point
        self.direction = direction

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        Line3D of direction Vector3D(1.0, 2.0, 3.0) passing through Point3D(0.0, 0.0, 0.0)
        """
        return 'Line3D of direction %s passing through %s' %\
               (self.direction, self.point)

    def __contains__(self, point):
        r"""
        Returns True if and only if the given point lies on the line.

        >>> line = Line3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        >>> Point3D(-1.0, -2.0, -3.0) in line
        True
        >>> Point3D(0.0, 0.0, 0.0) in line
        True
        >>> Point3D(1.0, 3.0, 2.0) in line
        False
        """
        return self.direction.has_same_direction(point - self.point)

    def intersection_with(self, other):
        r"""
        Returns the intersection of self with other.

        There are three possible scenarios:

        - The two lines coincide, so the intersection is a line;
        - The two lines intersect in a single point;
        - The two lines do not intersect.

        >>> point1 = Point3D(4.0, 5.0, -2.0)
        >>> point2 = Point3D(6.0, 5.0, 2.5)
        >>> point3 = Point3D(-2.0, 1.0, 4.0)
        >>> vector1 = Vector3D(1.0, 2.0, 3.0)
        >>> vector2 = Vector3D(-1.0, 2.0, -1.5)
        >>> vector3 = Vector3D(-1.0, 2.0, -4.0)
        >>> line1 = Line3D(point1, vector1)
        >>> line2 = Line3D(point2, vector2)
        >>> line3 = Line3D(point3, vector3)
        >>> line4 = Line3D(point1, vector2)
        >>> line1.intersection_with(line1)
        Line3D of direction Vector3D(1.0, 2.0, 3.0) passing through Point3D(4.0, 5.0, -2.0)
        >>> line1.intersection_with(line2)
        Point3D(5.0, 7.0, 1.0)
        >>> line1.intersection_with(line3) is None
        True
        >>> line1.intersection_with(line4)
        Point3D(4.0, 5.0, -2.0)
        """
        if self.point in other:
            if other.point in self and self.direction.has_same_direction(other.direction):
                return self
            else:
                return self.point
        else:
            # https://math.stackexchange.com/questions/270767/find-intersection-of-two-3d-lines
            g = other.point - self.point
            fxg = other.direction.cross_product(g)
            fxe = other.direction.cross_product(self.direction)
            if not fxe.has_same_direction(fxg): return None
            l = (fxg.norm() / fxe.norm()) * self.direction
            sign = 1 if fxg.has_same_direction(fxe, True) else -1
            return self.point + sign * l

class LineSegment3D():
    """
    """
    def __init__(self, point0, point1):
        self.p0 = point0
        self.p1 = point1
        self._direction = self.p1 - self.p0

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> LineSegment3D(Point3D(0.0, 0.0, 0.0), Point3D(1.0, 2.0, 3.0))
        LineSegment3D beginning from Point3D(0.0, 0.0, 0.0), ending at Point3D(1.0, 2.0, 3.0)
        """
        return 'LineSegment3D beginning from %s, ending at %s' % (self.p0, self.p1)

    def __contains__(self, point):
        r"""
        Tells if the given point is on the line segment.

        >>> line = LineSegment3D(Point3D(0.0, 0.0, 0.0), Point3D(1.0, 2.0, 3.0))
        >>> Point3D(1.0, 3.0, 2.0) in line
        False
        >>> Point3D(-1.0, -2.0, -3.0) in line
        False
        >>> Point3D(0.0, 0.0, 0.0) in line
        True
        >>> Point3D(0.5, 1.0, 1.5) in line
        True
        """
        diffs = [a-b for (a,b) in zip([point]*2, [self.p0, self.p1])]
        return all([d.norm() <= self._direction.norm() for d in diffs]) and diffs[0].has_same_direction(self._direction)

    def intersection_with(self, other):
        r"""
        Returns the intersection of self with other.

        There are three possible scenarios:

        - The two segments coincide, so the intersection is a segment;
        - The two segments intersect in a single point;
        - The two segments do not intersect.

        >>> point1 = Point3D(4.0, 5.0, -2.0)
        >>> point2 = Point3D(6.0, 5.0, 2.5)
        >>> point3 = Point3D(-2.0, 2.0, 3.0)
        >>> point4 = Point3D(2.5, 1.0, 6.0)
        >>> point5 = Point3D(-1.0, 2.0, -1.5)
        >>> segment1 = LineSegment3D(point1, point2)
        >>> segment2 = LineSegment3D(point1, point3)
        >>> segment3 = LineSegment3D(point4, point5)
        >>> segment1.intersection_with(segment1)
        LineSegment3D beginning from Point3D(4.0, 5.0, -2.0), ending at Point3D(6.0, 5.0, 2.5)
        >>> segment1.intersection_with(segment3) is None
        True
        >>> segment1.intersection_with(segment2)
        Point3D(4.0, 5.0, -2.0)
        """
        intersection = Line3D(self.p0, self._direction).intersection_with(Line3D(other.p0, other._direction))
        if isinstance(intersection, Line3D):
            # Les segments sont alignées, mais pas nécessairement intersectés
            right_operands = [self.p0]*2 + [self.p1]*2
            diffs = [a-b for (a,b) in zip([other.p0, other.p1]*2, right_operands)]
            dmax,_ = max([(d,d.norm()) for d in diffs], key=lambda x: x[1])
            for d,p in zip(diffs, right_operands):
                # Orientation différente
                if not d.has_same_direction(dmax, True):
                    return LineSegment3D(p+d, p)
            # Les segments ne s'intersectent pas
            return None
        elif isinstance(intersection, Point3D) and intersection in self and intersection in other:
            return intersection
        else:
            return None

class HalfLine3D():
    """
    """
    def __init__(self, point, direction):
        self.point = point
        self.direction = direction

    def __repr__(self):
        r"""
        Returns a string representation of self.

        >>> HalfLine3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        HalfLine3D of direction Vector3D(1.0, 2.0, 3.0) beginning at Point3D(0.0, 0.0, 0.0)
        """
        return 'HalfLine3D of direction %s beginning at %s' % (self.direction, self.point)

    def __contains__(self, point):
        r"""
        Returns True if and only if the given point lies on the line.

        >>> line = HalfLine3D(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 2.0, 3.0))
        >>> Point3D(-1.0, -2.0, -3.0) in line
        False
        >>> Point3D(0.0, 0.0, 0.0) in line
        True
        >>> Point3D(1.0, 3.0, 2.0) in line
        False
        """
        return self.direction.has_same_direction(point - self.point, True)

    def intersection_with(self, other):
        r"""
        Returns the intersection of self with other.

        There are three possible scenarios:

        - The two lines coincide, so the intersection is a line;
        - The two lines intersect in a single point;
        - The two lines do not intersect.
        >>> point1 = Point3D(4.0, 5.0, -2.0)
        >>> point2 = Point3D(6.0, 5.0, 2.5)
        >>> point3 = Point3D(-2.0, 1.0, 4.0)
        >>> vector1 = Vector3D(1.0, 2.0, 3.0)
        >>> vector2 = Vector3D(-1.0, 2.0, -1.5)
        >>> vector3 = Vector3D(-1.0, 2.0, -4.0)
        >>> line1 = HalfLine3D(point1, vector1)
        >>> line2 = HalfLine3D(point2, vector2)
        >>> line3 = HalfLine3D(point3, vector3)
        >>> line1.intersection_with(line1)
        HalfLine3D of direction Vector3D(1.0, 2.0, 3.0) beginning at Point3D(4.0, 5.0, -2.0)
        >>> line1.intersection_with(line2)
        Point3D(5.0, 7.0, 1.0)
        >>> line1.intersection_with(line3) is None
        True
        """
        intersection = Line3D(self.point, self.direction).intersection_with(Line3D(other.point, other.direction))
        if isinstance(intersection, Line3D):
            return other if other.point in self else (self if self.point in other else None)
        elif isinstance(intersection, Point3D) and intersection in self and intersection in other:
            return intersection
        else:
            return None

class Torus():
    """
    A torus. The cartesian equation of a torus:

    (sqrt((x-a)**2+(y-b)**2) - R)**2 + (z-c)**2 = r**2
    """

    torus_left_part = lambda x,y,z,a,b,c,R: (sqrt((x-a)**2+(y-b)**2) - R)**2 + (z-c)**2
    def __init__(self, center, small_radius, big_radius):
        self.center       = center
        self.small_radius = small_radius
        self.big_radius   = big_radius

    def meets(self, point):
        r"""
        Tells whether a point is on the torus' surface. It does iff the torus'
        equation is satisfied by the given point.
        """
        x = point.x; y = point.y; z = point.z;
        a = self.center.x; b = self.center.y; c = self.center.z;
        return torus_left_part(x,y,z,a,b,c,self.big_radius) == self.small_radius**2

    def surrounds(self, point):
        r"""
        Tells whether a point is surrounded by the torus, i.e. it is inside.
        """
        x = point.x; y = point.y; z = point.z;
        a = self.center.x; b = self.center.y; c = self.center.z;
        return torus_left_part(x,y,z,a,b,c,self.big_radius) - self.small_radius**2 < 0


# vim:set et sw=4 ts=4 tw=120:

